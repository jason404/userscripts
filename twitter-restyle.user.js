
// ==UserScript==
// @name         Twitter Restyle
// @namespace    https://bitbucket.org/jason404/userscripts/raw/master/twitter-restyle.user.js
// @version      1.4
// @description  Changes the font and adds navigation elements
// @author       eggbean
// @match        https://twitter.com/*
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';

    // Change this section with your custom links
    const link_configs = [
        {
            'name': "Cloud Computing",
            'link': "/i/topics/898648511855550464"
        },
        {
            'name': "Becky Clough",
            'link': "/Becky_Clough"
        }

    ]

    const search_query = '[aria-label="Primary"][role="navigation"]';

    function monitorPageForChanges() {
        const targetNode = document.getElementsByTagName('body')[0];
        const config = { attributes: false, childList: true, subtree: true };
        const observer = new MutationObserver(managePageChanges);
        observer.observe(targetNode, config);
    }

    function managePageChanges(mutations, observer) {
        for (const mutation of mutations) {
            for (const node of mutation.addedNodes) {
                if (node instanceof HTMLElement) {
                    manageNavbar(node, observer);
                }
            }
        }
    }

    function manageNavbar(node, observer) {
        const primary_navbar = node.querySelector(search_query);
        if (primary_navbar) {
            observer.disconnect();
            addLinks(primary_navbar);
        }
    }

    function addLinks(parent_elem) {
        const reference_node = parent_elem.querySelector('[aria-label="Profile"][role="link"]')
        let new_links = generateNewLinks(reference_node);

        new_links.forEach((new_link) => {
            parent_elem.insertBefore(new_link, reference_node.nextSibling);
        })

    }

    function generateNewLinks(reference_node) {
        // const ref_classes = reference_node.classList;
        let new_links = [];

        link_configs.forEach((config) => {
            let link_elem = reference_node.cloneNode(true);
            let correct_name_css = getNameCSS();
            let correct_svg_css = getSvgCSS();

            let link_name_elem = link_elem.querySelector('div div span');
            let link_name_container = link_name_elem.parentNode;
            let link_svg_elem = link_elem.querySelector('div > div > svg');

            link_elem.setAttribute('href', config.link);
            link_elem.setAttribute('aria-label', 'custom_link');
            link_name_elem.innerText = config.name;
            link_name_container.className = correct_name_css;
            console.log(correct_svg_css)
            link_svg_elem.setAttribute('class', correct_svg_css);

            new_links.push(link_elem);
        })

        return new_links;
    }

    function getNameCSS() {
        let example_elem = document.querySelector('div[aria-label="More menu items"] div[dir="auto"]');
        return example_elem.className;
    }

    function getSvgCSS() {
        let example_svg = document.querySelector('div[aria-label="More menu items"] svg');
        return example_svg.getAttribute('class');
    }

    monitorPageForChanges();

    function addGlobalStyle(css) {
        var head, style;
        head = document.getElementsByTagName('head')[0];
        if (!head) { return; }
        style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = css;
        head.appendChild(style);
    }

    addGlobalStyle('.r-37j5jr { font-family: "Fira Sans Book", TwitterChirp, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif !important; }');

    (function() {
        var gKeyPressed = false;
        addEventListener("keydown", function(e){
            if (document.activeElement.tagName != 'INPUT' && document.activeElement.tagName != 'TEXTAREA' && document.activeElement.contentEditable != 'true') {
                switch (e.keyCode) {
                    case 71: //"g"
                        gKeyPressed = true;
                        break;
                    case 72: //"h"
                        if (e.keyCode === 72 && gKeyPressed === false) {
                            history.back();
                        }
                        break;
                    default:
                        gKeyPressed = false;
                        break;
                }
            }
        });
    })();


})();

