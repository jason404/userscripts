// ==UserScript==
// @name         Rain Alarm Cleanup
// @namespace    https://bitbucket.org/jason404/userscripts/raw/master/rainalarm-cleanup.user.js
// @version      0.2a
// @description  Removes the advert sidebar
// @author       eggbean
// @match        https://www.rain-alarm.com/*
// @grant        none
// ==/UserScript==

(function () {
    var callback = function (data, mo) {
        function addGlobalStyle(css) {
            var head, style;
            head = document.getElementsByTagName('head')[0];
            if (!head) { return; }
            style = document.createElement('style');
            style.type = 'text/css';
            style.innerHTML = css;
            head.appendChild(style);
        }

        addGlobalStyle('#map_canvas.element.style { width: 100% !important; }');
        addGlobalStyle('#topbarCenter.element.style { width: 100% !important; }');
    };
    var mo = new MutationObserver(callback);
    mo.observe(document.body, {
        childList: true,
        subtree: true
    });
})();
